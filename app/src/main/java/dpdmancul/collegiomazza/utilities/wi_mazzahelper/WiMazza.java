package dpdmancul.collegiomazza.utilities.wi_mazzahelper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.widget.Toast;
import android.util.Log;
import androidx.annotation.NonNull;
import java.net.URLEncoder;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by dpd- on 06/12/17.
 */

public class WiMazza {
  private WiMazza(){}

  public static void login(String username,String password,final Context context){
    // Instantiate the RequestQueue.
    RequestQueue queue = Volley.newRequestQueue(context);

    String url;
    try {
      url = "http://mt.wiffi.it/login?username=" + URLEncoder.encode(username, "utf-8") + "&password=" + URLEncoder.encode(password, "utf-8");
    }catch(Exception e){
      url = "http://mt.wiffi.it/login?username=" + username + "&password=" + password;
    }
    /*RISOLVE I PROBLEMI PER I NUOVI ANDROID*/
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
      ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
      for (Network net : connectivityManager.getAllNetworks()) {
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(net);
        if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
          connectivityManager.bindProcessToNetwork(net);
          break;
        }
      }
    }
    /*FINE RISOLUZIONE PROBLEMI*/

    // Request a string response from the provided URL.
    StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
      new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
          WiMazza.logged(context,
              ()->Toast.makeText(context.getApplicationContext(), "Ciao "+WiMazza.getCredentials(context).getUsername()+"! Benvenuto in Wi-Mazza \uD83D\uDE03", Toast.LENGTH_SHORT).show(),
              ()->Toast.makeText(context.getApplicationContext(), "Orpo! Hai sbagliato le credenziali \u2639\uFE0F", Toast.LENGTH_SHORT).show());
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          Log.d("INFO",error.toString());
          Toast.makeText(context.getApplicationContext(), "Orpo! Errore di autenticazione a Wi-Mazza \u2639\uFE0F", Toast.LENGTH_SHORT).show();
      }
    });
    // Add the request to the RequestQueue.
    queue.add(stringRequest);
  }
  public static void login(Credentials credenziali,Context context) {
    login(credenziali.getUsername(),credenziali.getPassword(),context);
  }
  public static void logout(Context context){
    // Instantiate the RequestQueue.
    RequestQueue queue = Volley.newRequestQueue(context);
    String url = "http://mt.wiffi.it/logout";

    // Request a string response from the provided URL.
    StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
      new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
          logged(context,
              ()->Toast.makeText(context.getApplicationContext(), "Mhm sembra che tu sia ancora connesso \uD83E\uDD14", Toast.LENGTH_SHORT).show(),
              ()->Toast.makeText(context.getApplicationContext(), "Arrivederci da Wi-Mazza \uD83D\uDC4B", Toast.LENGTH_SHORT).show());
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          Log.d("INFO",error.toString());
          Toast.makeText(context.getApplicationContext(), "Orpo! Errore di logout da Wi-Mazza \u2639\uFE0F", Toast.LENGTH_SHORT).show();
        }
    });
    // Add the request to the RequestQueue.
    queue.add(stringRequest);
  }

  public static void logged(Context context, Runnable loggedR, Runnable notLoggedR, Runnable errorR){
    try {
      MainActivity activity = (MainActivity)context;
      _logged(context, () -> {
        loggedR.run();
        activity.logged(false, true);
      }, () -> {
        notLoggedR.run();
        activity.logged(false, false);
      }, () -> {
        errorR.run();
        activity.logged(false, false);
        // activity.logged(true);
      });
    }catch(Throwable e){
      _logged(context,loggedR,notLoggedR,errorR);
    }
  }
  public static void logged(Context context, Runnable loggedR, Runnable notLoggedR){
    logged(context,loggedR,notLoggedR,()->{Log.d("ERROR","In logged: error code not 302"); notLoggedR.run();});
  }
  private static void _logged(Context context, Runnable loggedR, Runnable notLoggedR, Runnable errorR ){
    //mt.wiffi.it/status error 302 = not logged
    RequestQueue queue = Volley.newRequestQueue(context);
    String url = "http://mt.wiffi.it/status";
    StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
          loggedR.run();
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          if(error!=null && error.networkResponse!=null && error.networkResponse.statusCode == 302) {
            notLoggedR.run();
          }else {
            errorR.run();
          }
        }
    });
    // Add the request to the RequestQueue.
    queue.add(stringRequest);
  }

  public static class Credentials{
    Credentials(String user, String pass){
      username = user;
      password = pass;
    }
    public String getUsername(){return username;}
    public String getPassword(){return password;}

    private String username,password;
  }


  public static void saveCredentials(String user, String pass,Context context){
    SharedPreferences.Editor pref = context.getSharedPreferences(WI_MAZZA_CREDENTIALS,Context.MODE_PRIVATE).edit();
    pref.putString("username",user);
    pref.putString("password",pass);
    pref.apply();
  }
  public static void saveCredentials(Credentials credenziali,Context context){
    saveCredentials(credenziali.getUsername(),credenziali.getPassword(),context);
  }
  @NonNull
  public static Credentials getCredentials(Context context){
    SharedPreferences pref = context.getSharedPreferences(WI_MAZZA_CREDENTIALS,Context.MODE_PRIVATE);
    return new Credentials(pref.getString("username",""),pref.getString("password",""));
  }

  private static final String WI_MAZZA_CREDENTIALS = "WiMazzaHelper.Credentials";
}
