package dpdmancul.collegiomazza.utilities.wi_mazzahelper;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import androidx.annotation.NonNull;
import android.util.Log;

import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;


public class WifiReceiver extends Worker {

  private Context context = this.getApplicationContext();

  public WifiReceiver(
      @NonNull Context context,
      @NonNull WorkerParameters params) {
    super(context, params);
  }

  @Override
  public Result doWork() {
    Log.d("INFO","Worker partito");
    autoLogin(context);
    WifiReceiver.workStarter(WifiAntiReceiver.class, NetworkType.METERED);
    return Result.success();
   }

  static void workStarter(Class wc, NetworkType nt){
    Constraints constraints = new Constraints.Builder()
        .setRequiredNetworkType(nt)
        .build();
    OneTimeWorkRequest otr =
        new OneTimeWorkRequest.Builder(wc)
            .setConstraints(constraints)
            .build();
    String name = wc.getName();
    Log.d("INFO","Incodato: "+name);
    WorkManager.getInstance().enqueueUniqueWork(name, ExistingWorkPolicy.KEEP,otr);
  }

  static void autoLogin(Context context){
  WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService (Context.WIFI_SERVICE);
  WifiInfo info = wifiManager.getConnectionInfo ();
  Log.d("INFO",info.getSSID());
  if(info.getSSID().equals("\"Wi-Mazza\""))
    WiMazza.logged(context,()->{}, ()->WiMazza.login(WiMazza.getCredentials(context),context));
  }
}