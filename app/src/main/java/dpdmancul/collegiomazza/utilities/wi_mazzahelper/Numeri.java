package dpdmancul.collegiomazza.utilities.wi_mazzahelper;

/**
 * Created by dpd-
 */

public class Numeri {
  enum Edificio{
    A(5),C(6),M(4),F(2);

    private int value;
    Edificio(int value) {
        this.value = value;
    }
    public int getValue(){return value;}
  }

  public static Edificio[] edifici = {Edificio.A,Edificio.M,Edificio.C,Edificio.F};
  public static String[][] piani = {
      {"Terra","1","2","3","4","5","6"},
      {"Terra","1","2","3"},
      {"","1"},
      {"Terra","1","2","3","4","5"}
  };
  public static int[][][] stanze={
    {//A
      {-13,-12,-11,16,67,68},
      {0,2,3,4,5,6,7,8,9,10,11,15,17,18},
      {0,1,2,3,4,5,6,7,8,9,10},
      {0,1,2,3,4,5,6,7,8,9,10},
      {0,1,2,3,4,5,6,7,8,9,10},
      {0,1,2,3,4,5,6,7,8,9,10},
      {0,1,2,3}
    },
    {//M
      {0},
      {0,1},
      {0,1,2,3,4,5,6,7,8,9,10,11},
      {0,1,2,3,4,5,6,7,8,9,10,11,12,14}
    },
    {//C
      {},
      {0,1,2,3,4,5,6,7,8,9,10,11,12,13}
    },
    {//F
      {10,20,58,1,2,3,4,5,6,7,8,9},
      {1,2,3,4,5,6,7,8,9},
      {1,2,3,4,5,6,7,8,9},
      {1,2,3,4,5,6,7,8,9},
      {1,2,3,4,5,6,7,8},
      {2,4,6}
    }
  };

  public static String getName(Edificio edificio,int piano,int stanza){
    switch(edificio){
      case A:
        switch(stanza){
          case -13: return "Bar";
          case -12: return "Mensa";
          case -11: return "Cucina";
          case 67: return "Biglietteria";
          case 68: return "Cabina regia";
          case 10: return "Coffee ".concat(Integer.toString(piano));
          case 11: return "Porco studio";
          case 16: return "Lavanderia";
        }
        switch(piano){
          case 1:
            if(stanza == 1) return "A100";
            break;
          case 6:
            switch(stanza){
              case 6: return "Biglietteria";
              case 7: return "Cabina regia";
            }
            break;
          case 7: return "Mensa";
          case 8: return "Cucina";
        }
        break;
      case C:
        if(stanza%100 == 0) return "Atrio";
        break;
      case M:
        switch(piano){
          case 0: return "Portineria";
          case 1:
            String[] a = {"Aula Info","Aula ECDL"};
            return a[stanza];
          case 2:
            String[] b = {"Aula L. Gui","Sala tesi","Biblioteca","Atrio","Sala del Consiglio","Attività culturali","Segretario generale","Segreteria","Amministrazione","Vicedirettore","Direttore generale","Direttore"};
            return b[stanza];
          case 3:
            switch(stanza){
              case 8: return "Aula studio";
              case 9: return "Coffee M";
              case 10: return "Atrio";
            }
        }
        break;
      case F:
        if (piano == 0 && stanza > 9){
          String[] a = {"","Segreteria","Direttrice","","","Custode"};
          return a[stanza / 10];
        }
        return Integer.toString(10*piano+stanza);
    }
    return edificio.toString().concat(Integer.toString(100*piano+stanza));
  }

  public static class Stanza{
    Stanza(Edificio e,int p,int s){
      edificio=e;
      piano=p;
      stanza=s;
    }
    public Stanza move(int piano, int stanza){return new Stanza(edificio,piano,stanza);}
    public Edificio getEdificio(){return edificio;}
    public int getPiano(){return piano;}
    public int getStanza(){return stanza;}
    public String toString(){return getName(edificio,piano,stanza);}

    private Edificio edificio;
    private int piano, stanza;
  }

  static private int assembly(Stanza s){
    return 100*s.getEdificio().getValue()+10*s.getPiano()+s.getStanza();
  }

  static private int getA(Stanza s){
    int stanza = s.getStanza();
    int piano = s.getPiano();
    if(piano == 6)
      stanza++;
    if(piano == 1){
      if(stanza == 15)
        return 531;
      if(stanza == 17 || stanza == 18)
        return 532;
      piano = 2;
      if(stanza == 0)
        stanza = 1;
    }else if(stanza == 0)
      piano = 0;
    else
      piano += 2;
    if(stanza == 0)
      stanza = s.getPiano();
    else
      stanza--;
    return assembly(s.move(piano,stanza));
  }

  static private int[] numeriC = {674,677,676,675,688,687,686,685,684,683,682,681,679,680};
  static private int[][] numeriM = {
    {411},
    {403,423},
    {421,424,426,428,430,431,432,434,435,437,465,480},
    {460,466,420,422,425,429,427,459,458,457,451,450,450,0,463}
  };

  static int get(Stanza s){
    switch(s.getEdificio()){
      case A:
        return getA(s);
      case C:
        return numeriC[s.getStanza()];
      case M:
        return numeriM[s.getPiano()][s.getStanza()];
      case F:
        return assembly(s);
    }
    return -1;
  }

  static int get(Edificio edificio,int piano,int stanza){
    return get(new Stanza(edificio,piano,stanza));
  }
  static int get(String stanza){
    return get(Edificio.valueOf(stanza.substring(0,1)),Integer.parseInt(stanza.substring(1,2)),Integer.parseInt(stanza.substring(2)));
  }
  static String getCall(Stanza s){
    return "+390498734".concat(Integer.toString(get(s)));
  }
  static String getCall(Edificio edificio,int piano,int stanza){
    return getCall(new Stanza(edificio,piano,stanza));
  }
  static String getCall(String stanza){
    return getCall(Edificio.valueOf(stanza.substring(0,1)),Integer.parseInt(stanza.substring(1,2)),Integer.parseInt(stanza.substring(2)));
  }
}
