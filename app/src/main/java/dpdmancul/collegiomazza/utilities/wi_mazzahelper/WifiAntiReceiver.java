package dpdmancul.collegiomazza.utilities.wi_mazzahelper;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import androidx.work.NetworkType;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class WifiAntiReceiver extends Worker {

  public WifiAntiReceiver(
      @NonNull Context context,
      @NonNull WorkerParameters params) {
    super(context, params);
  }

  @Override
  public Result doWork() {
    Log.d("INFO","Antiworker partito");
    WifiReceiver.workStarter(WifiReceiver.class, NetworkType.UNMETERED);
    return Result.success();
  }
}
