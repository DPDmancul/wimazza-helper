package dpdmancul.collegiomazza.utilities.wi_mazzahelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.work.NetworkType;

public class StartWifiReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
    Log.d("INFO", "Broadcast partito");
    Log.d("INFO", "Intent: " + intent);
    WifiReceiver.workStarter(WifiReceiver.class, NetworkType.UNMETERED);
  }
}
