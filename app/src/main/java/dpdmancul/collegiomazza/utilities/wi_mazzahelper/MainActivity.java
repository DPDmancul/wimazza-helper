package dpdmancul.collegiomazza.utilities.wi_mazzahelper;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity {

  Button loginButton, saveButton, logoutButton;
  EditText loginUsername, loginPassword;
  Spinner spinnerPiano,spinnerStanza;
  TextView textViewNumero;
  View layoutRisultato;


  /**
   * Called when the user taps the Login button
   */
  public void tryLogin(View view) {
    WiMazza.login(WiMazza.getCredentials(this),this);
  }
  /**
   * Called when the user taps the Logout button
   */
  public void tryLogout(View view) {
    WiMazza.logout(this);
  }

  /**
   * Called when the user taps the Save button
   */
  public void saveCredentials(View view) {
    final String username = loginUsername.getText().toString();
    final String password = loginPassword.getText().toString();

    WiMazza.saveCredentials(username,password,this);

    loginButton.setText("Login come "+username);
    loginButton.setEnabled(true);
  }

  public void logged(boolean error){
    logged(error,false);
  }
  public void logged(boolean error,boolean logged){
    if(error||!logged)
      loginButton.setVisibility(View.VISIBLE);
    if(error||logged)
      logoutButton.setVisibility(View.VISIBLE);
    if(!error&&logged)
      loginButton.setVisibility(View.GONE);
    if(!error&&!logged)
      logoutButton.setVisibility(View.GONE);
  }





  /// NUMERI DI TELEFONO
  private int edificio = 0, piano = 0, stanza = 0;
  private String numero = "";


  private void subselectPiano (){
    int[] intArray = Numeri.stanze[edificio][piano];
    String stanze[] = new String[intArray.length+1];
    stanze[0] = "Stanza ...";
    for (int i = 0; i < intArray.length; i++)
      stanze[i+1] = Numeri.getName(Numeri.edifici[edificio],piano,intArray[i]);

    ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, stanze);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinnerStanza.setAdapter(adapter);
    spinnerStanza.setVisibility(View.VISIBLE);
  }

  public void selectEdificio(View view) {
    switch(view.getId()){
      case R.id.buttonA:
        edificio = 0;
        break;
      case R.id.buttonM:
        edificio = 1;
        break;
      case R.id.buttonC:
        edificio = 2;
        break;
      case R.id.buttonF:
        edificio = 3;
    }
    ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,
        Numeri.piani[edificio]);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinnerPiano.setAdapter(adapter);
    piano = 0;
    if(edificio == 2) {//C
      spinnerPiano.setVisibility(View.GONE);
      piano = 1;
    }else{
      spinnerPiano.setVisibility(View.VISIBLE);
    }
    subselectPiano();
  }

  private class SpinnerPianoActivity extends Activity implements AdapterView.OnItemSelectedListener {
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
      piano = pos;
      subselectPiano();
    }
    public void onNothingSelected(AdapterView<?> parent) {}
  }
  private class SpinnerStanzaActivity extends Activity implements AdapterView.OnItemSelectedListener {
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
      if(pos == 0){ layoutRisultato.setVisibility(View.INVISIBLE); return;}
      stanza = Numeri.stanze[edificio][piano][pos-1];
      textViewNumero.setText(Integer.toString(Numeri.get(Numeri.edifici[edificio],piano,stanza)));
      numero = "tel:".concat(Numeri.getCall(Numeri.edifici[edificio],piano,stanza));
      layoutRisultato.setVisibility(View.VISIBLE);
    }
    public void onNothingSelected(AdapterView<?> parent) {}
  }

  private void subCall(){
    this.startActivity(new Intent(Intent.ACTION_CALL,Uri.parse(numero)));
  }
  private static final int CALL_PHONE_PERMISSION = 1;

  public void onCall(View v){
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PHONE_PERMISSION);
    else
      subCall();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    switch (requestCode) {
      case CALL_PHONE_PERMISSION:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
          subCall();
        break;
    }
  }



  //////////
  //OnCreate
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    this.sendBroadcast(new Intent(this, StartWifiReceiver.class).setAction("dpdmancul.collegiomazza.utilities.wi_mazzahelper.appStart"));


    loginButton = findViewById(R.id.loginButton);
    logoutButton = findViewById(R.id.logoutButton);
    saveButton = findViewById(R.id.saveButton);
    loginUsername = findViewById(R.id.loginUsername);
    loginPassword = findViewById(R.id.loginPassword);

    spinnerPiano = findViewById(R.id.spinnerPiano);
    spinnerStanza = findViewById(R.id.spinnerStanza);
    textViewNumero = findViewById(R.id.textViewNumero);
    layoutRisultato = findViewById(R.id.layoutRisultato);

    spinnerPiano.setOnItemSelectedListener(new SpinnerPianoActivity());
    spinnerStanza.setOnItemSelectedListener(new SpinnerStanzaActivity());


    //Controlla i permessi
    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
    dlgAlert.setTitle("Wi-Mazza Helper");
    dlgAlert.setCancelable(true);
    String msgRiavvio = "IMPORTANTE:\nPer far funzionare il login automatico devi abilitare l'avvio automatico e disabilitare il risparmio energetico per quest'app";
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      dlgAlert.setPositiveButton("OK", (DialogInterface dialog, int which)-> ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 0));
      dlgAlert.setMessage("È necessario fornire il permesso alla localizzazione in modo che l'applicazione sappia quando ci si connette a Wi-Mazza.\n"+msgRiavvio);
      dlgAlert.create().show();
    }else if(WiMazza.getCredentials(this).getUsername().equals("")){
      dlgAlert.setPositiveButton("OK", (DialogInterface dialog, int which)->{});
      dlgAlert.setMessage(msgRiavvio);
      dlgAlert.create().show();
    }


    if (!WiMazza.getCredentials(this).getUsername().equals("")) {
      loginButton.setText("Login come " + WiMazza.getCredentials(this).getUsername());
      loginButton.setEnabled(true);
      WifiReceiver.autoLogin(this);
    }

    TextWatcher loginDataWatcher = new TextWatcher() {
      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (loginUsername.getText().toString().equals("") || loginPassword.getText().toString().equals(""))
          saveButton.setEnabled(false);
        else
          saveButton.setEnabled(true);
      }

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void afterTextChanged(Editable s) {
      }
    };
    loginUsername.addTextChangedListener(loginDataWatcher);
    loginPassword.addTextChangedListener(loginDataWatcher);
  }

  private int eggs = 0;
  private long last = 0;
  public void onEaster(View v) {
    if(System.currentTimeMillis()-last>500)
      eggs=0;
    eggs++;
    last = System.currentTimeMillis();
    if(eggs==5)
      MainActivity.this.startActivity(new Intent(MainActivity.this, GranducatoActivity.class));
  }
}
