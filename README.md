# Wi-Mazza Helper

    Automatically login to Wi-Mazza: the Wiffi based wi-fi captive portal used by Collegio Mazza in Padua.

Effettua automaticamente il login a Wi-Mazza

1. Inserisci le tue credenziali di Wi-Mazza
2. Salva le credenziali
3. Dimenticati che esiste l'app: ogni volta che ti colleghi a Wi-Mazza il login avverrà in automatico!
- In caso in cui necessiti di rieffettuare il login puoi usare l'apposito pulsante nell'applicazione

**ATTENZIONE**  
I telefoni bloccano le applicazioni in background in automatico. Questa applicazione per poter funzionare deve poter attivarsi in background pertanto bisogna disattivare il blocco dal menù apposito (di solito nelle impostazioni risparmio energetico o impostazioni applicazioni) e abilitare l'avvio automatico.

