# Wi-Mazza helper privacy policy

This app requires access to coarse location in order to read the SSID (the name)
of the Wi-Fi network, in order to detect if you are connected to Wi-Mazza and so
do the automatic login.

This app does not collect any personal data. The credentials are stored locally
on your device and they never leave it.

