#!/bin/sh

cd $(dirname "$0")

gradle bundleRelease

cd app/build/outputs/bundle/release
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore /home/dpd-/Documenti/NextCloud/AndridStudioApkSignature\ \(old\).jks -signedjar app-release-signed.aab  app-release.aab key0
